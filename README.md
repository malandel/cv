## Adresse de la page :

https://malandel.gitlab.io/cv/


## Commentaires :

J'ai tenté d'utiliser Bootstrap pour avoir des colonnes responsives (classe .col-sm), à partir de la ligne 44 à 70.
Cela ne semble pas fonctionner.

La console du navigateur m'indique "Nom de propriété invalide" pour "-ms-flex-preferred-size : 0" et "-ms-flex-positive : 1"

J'étudierai ce problème plus tard.

